#OpenUI5 Challenge

Welcome to the objective partner Job Application Challenge!

Your task is to create a simple TODO app using OpenUI5. The app should display a list of TODOs
along with a description for the chosen TODO (created date, description, title). There should be also a possibility to add and save new TODOs. Some data should be already available on the backend side. To win bonus points you can add a feature of your wish to make your project really outstanding.

The data should be loaded from a locally hosted express server and use OData Protocol for communication.

To provide a backend service you can use one of the following npm packages:

* https://www.npmjs.com/package/odata-v4-server
* https://www.npmjs.com/package/simple-odata-server

Please fork this repo and when you are done, create a pull request with the finished project.

To evaluate your solution we will take into consideration the overall code quality, unit tests, required features and git proficiency. 

You have 48 hours to complete the challenge.

Good luck!